# README #

My presentation materials. 

### What is this repository for? ###
https://docs.google.com/presentation/d/1wHB0qgYrj_JzlATxQxBAv95_rBRRK1fXKvJrG0lnJUE/edit?usp=sharing
Link to my Google Slides Presentation.

### How do I get set up? ###
The only file in the repo is the source code for my demo as well as the iPython notebook file, if you would like to open the Jupyter notebook file the least painless way to do this is to download the anaconda distribution, but it is still a slightly lengthy process.

https://www.continuum.io/downloads