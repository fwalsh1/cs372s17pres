
# coding: utf-8

# In[1]:

import numpy as np


# In[2]:

import pandas as pd


# In[3]:

import sklearn


# In[5]:

from sklearn import tree


# In[6]:

from pandas import *


# In[7]:

import seaborn as sns


# In[15]:

import matplotlib.pyplot as plt


# In[16]:

df = pd.read_csv("movie_metadata.csv")


# In[17]:

df.head(5)


# In[18]:

df.dropna


# In[19]:


get_ipython().magic('matplotlib inline')

corr = df.select_dtypes(include = ['float64', 'int64']).iloc[:, 1:].corr()
plt.figure(figsize=(15, 15))
sns.heatmap(corr, vmax=1, square=True)


# In[ ]:




# In[ ]:



